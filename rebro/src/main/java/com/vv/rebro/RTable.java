package com.vv.rebro;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Abstract representation of generic Realm table
 */
public class RTable {

    @NonNull
    private String name;
    @NonNull
    private Object[][] values;
    @NonNull
    private String[] columnNames;
    @NonNull
    private RType[] columnTypes;
    @NonNull
    private String[] columnClasses;

    public RTable(@NonNull final String name, final int columns, final int rows) {
        this.name = name;
        values = new Object[columns][rows];
        columnNames = new String[columns];
        columnTypes = new RType[columns];
        columnClasses = new String[columns];
    }

    @Nullable
    public Object getValueAt(final int row, final int column) {
        return values[row][column];
    }

    public void setValueAt(@Nullable final Object value, final int column, final int row) {
        values[column][row] = value;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull final String name) {
        this.name = name;
    }

    @NonNull
    public Object[][] getValues() {
        return values;
    }

    public void setValues(@NonNull final Object[][] values) {
        this.values = values;
    }

    @NonNull
    public String[] getColumnNames() {
        return columnNames;
    }

    public void setColumnNames(@NonNull final String[] strings) {
        columnNames = strings;
    }

    public void setColumnName(final int i, @NonNull final String columnName) {
        columnNames[i] = columnName;
    }

    @NonNull
    public RType[] getColumnTypes() {
        return columnTypes;
    }

    public void setColumnTypes(@NonNull final RType[] columnTypes) {
        this.columnTypes = columnTypes;
    }

    public void setColumnType(final int columnIdx, @NonNull final RType type) {
        columnTypes[columnIdx] = type;
    }

    @NonNull
    public String[] getColumnClasses() {
        return columnClasses;
    }

    public void setColumnClasses(@NonNull final String[] columnClasses) {
        this.columnClasses = columnClasses;
    }

    public void setColumnClass(final int columnIdx, @NonNull final String name) {
        columnClasses[columnIdx] = name;
    }
}
