package com.vv.rebro;

public enum RType {
    INTEGER,
    BOOLEAN,
    STRING,
    BINARY,
    DATE,
    FLOAT,
    DOUBLE,
    OBJECT,
    LIST,
    UNSUPPORTED
}
