package com.vv.rebro;

public enum RpcMethod {
    CONNECT,
    LIST,
    PUSH,
    DISCONNECT
}
