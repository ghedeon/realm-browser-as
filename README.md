# Realm Browser for Android Studio

``app`` — example application

``rebro`` — actual Realm Browser android library

## Usage
Add ``rebro`` library as a dependency to your ``build.gradle`` file. Consult the example app.

No additional setup is required. 

## WebSockets communication

The ```rebro``` library is running a WS Client that connects to the WS Server owned by Rebro plugin.

A broadcast intent is sent by the Rebro plugin in order to communicate the server's IP to clients.
 
_Alternatively, an UDP autodiscovering technique might be used in order to identify the WS Server._ 

JSON RPC (backed by json-smart) is used in order to formalize the protocol and handle serialization/deserialization routine.

![](rebro_table.png)


### Libraries used
WebSockets: https://github.com/TooTallNate/Java-WebSocket

JSON RPC: http://software.dzhuvinov.com/json-rpc-2.0-base.html

Ideally, a smaller subset of RFC 6455 and JSON-RPC 2.0 Specification can be implemented, in order to reduce the number of dependencies.